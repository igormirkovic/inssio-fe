import {Component, EventEmitter, Input, Output, OnInit} from '@angular/core';
import {AppService} from '../app.service';
import {ToastService} from 'angular-toastify';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  @Input() formStatus: string;
  @Input() modalID: number;
  @Input() modelContext: string;
  @Input() model: any;
  @Output() onCloseModal = new EventEmitter();
  @Output() onGetData = new EventEmitter();


  constructor(private appService: AppService, private toastService: ToastService) {}

  ngOnInit(): void {}

  addStudent(): void {
    this.appService.addStudent(this.model).subscribe((res) => {
      this.toastService.info('Student added successfully.');
      this.onGetData.emit();
      this.onCloseModal.emit();
    });
  }

  addCourse(): void {
    this.appService.addCourse(this.model).subscribe((res) => {
      this.toastService.info('Course added successfully.');
      this.onGetData.emit();
      this.onCloseModal.emit();
    });
  }

  editStudent(): void {
    this.appService.editStudent(this.model).subscribe((res) => {
      this.toastService.info('Student edited successfully.');
      this.onGetData.emit();
      this.onCloseModal.emit();
    });
  }

  editCourse(): void {
    this.appService.editCourse(this.model).subscribe((res) => {
      this.toastService.info('Course edited successfully.');
      this.onGetData.emit();
      this.onCloseModal.emit();
    });
  }

  deleteCourse(): void {
    this.appService.deleteCourse(this.modalID).subscribe((res) => {
      this.toastService.info('Course deleted successfully.');
      this.onGetData.emit();
      this.onCloseModal.emit();
    });
  }

  deleteStudent(): void {
    this.appService.deleteStudent(this.modalID).subscribe((res) => {
      this.toastService.info('Course deleted successfully.');
      this.onGetData.emit();
      this.onCloseModal.emit();
    });
  }

}
