import {Component, OnInit} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {AppService, Course, Mark, Student} from './app.service';
import {mark} from '@angular/compiler-cli/src/ngtsc/perf/src/clock';
import {ToastService} from 'angular-toastify';
import {forkJoin, Observable} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [AppService]
})
export class AppComponent {
  model: any = {};
  students: [Student];
  courses: [Course];
  coursesFilter: [Course];
  studentsFilter: [Student];
  marks: [Mark];
  modalOpen = false;
  modalStatus = 'student';
  modalContext = 'edit';
  modalID: number;
  formattedArray: any;
  headerRow: any;
  title = 'inssio-app-FE';
  filterCourses: any = [];
  filterStudents: any = [];
  loading = true;


  constructor(private appService: AppService, private toastService: ToastService) {
    this.getData();
  }

  getStudents(): Promise<void> {
    return new Promise(resolve => {
      if (this.filterStudents.length){
        return this.appService.getStudentsByIDs(this.filterStudents).subscribe((res) => {
          this.students = res;
          resolve();
        });
      }else{
        return this.appService.getStudents().subscribe((res) => {
          this.students = res;
          this.studentsFilter = res;
          resolve();
        });
      }
    })

  }

  getCourses(): Promise<void> {
    return new Promise(resolve => {
      const headerRow = [{
        name: 'Name'
      }];
      if (this.filterCourses.length){
        this.appService.getCoursesByIDs(this.filterCourses).subscribe((res) => {
          this.courses = res;
          this.courses.map((obj) => {
            return headerRow.push({
              name: obj.name,
              ID: obj.courseID
            });
          });
        });
      }else{
        this.appService.getCourses().subscribe((res) => {
          this.courses = res;
          this.coursesFilter = res;
          this.courses.map((obj) => {
            return headerRow.push({
              name: obj.name,
              ID: obj.courseID
            });
          });
        });
      }
      this.headerRow = headerRow;
      resolve();
    })
  }

  getMarks(): void{
    this.appService.getMarks().subscribe((res) => {
      this.marks = res;
      this.filterMarks();
    });
  }

  filterMarks(): void{
    let studentGrade: any = {};
    const formattedArray = [];
    for (let i = 0; i < this.students.length; i++) {
      studentGrade.ID = this.students[i].studentID;
      studentGrade.Name = this.students[i].firstName + ' ' + this.students[i].lastName;
      for (let j = 0; j < this.courses.length; j++) {
        const mark = this.marks.find(mark =>
          mark.student.studentID === this.students[i].studentID && mark.course.courseID === this.courses[j].courseID );
        if(mark){
          studentGrade[this.courses[j].name] = {
            grade: mark.grade,
            courseID: this.courses[j].courseID,
            markID: mark.markID
          };
        }else{
          studentGrade[this.courses[j].name] = {
            grade: '/',
            courseID: this.courses[j].courseID
          };
        }
      }
      formattedArray.push(studentGrade);
      studentGrade = {};
    }
    this.formattedArray = formattedArray;
    this.loading = false;
  }

  getEnitites(): Promise<void>{
      return Promise.all([this.getStudents(), this.getCourses()]).then(() => {
        this.getMarks();
      });
  }

  getData(): void {
    this.getEnitites();
  }

  openModal(status): void {
    this.model = {};
    this.modalStatus = status;
    this.modalContext = 'add';
    this.modalOpen = true;
  }

  closeModal(): void {
    this.modalOpen = false;
  }

  getStudentDetail(row: Student): void{
    this.appService.getStudentByID(row.ID).subscribe((res) => {
      this.modalID = row.ID;
      this.model = res;
      this.modalOpen = true;
      this.modalStatus = 'student';
      this.modalContext = 'edit';
    });
  }

  getCourseDetail(row: Course): void{
    this.appService.getCourseByID(row.ID).subscribe((res) => {
      this.modalID = row.ID;
      this.model = res;
      this.modalOpen = true;
      this.modalStatus = 'course';
      this.modalContext = 'edit';
    });
  }

  addMark(row: any, studentID: any, $event: any): any{
    const obj = {
      StudentID: studentID,
      CourseID: row.courseID,
      grade: parseInt($event.target.value, 10)
    };
    this.appService.addMark(obj).subscribe((res) => {
      this.model = res;
      this.toastService.info('Mark added successfully.');
      this.getData();
    });
  }

  updateMark(markID: any, $event: any): any{
    const obj = {
      MarkID: markID,
      grade: parseInt($event.target.value, 10)
    };
    this.appService.editMark(obj).subscribe((res) => {
      this.model = res;
      this.toastService.info('Mark updated successfully.');
      this.getData();
    });
  }

  filterStudentsOnChange(event): any{
      if (event.source.selected){
        this.filterStudents.push(event.source.value);
      }else{
        const index = this.filterStudents.indexOf(event.source.value);
        if (index > -1) {
          this.filterStudents.splice(index, 1);
        }
      }
      this.getData();
  }

  filterCoursesOnChange(event): any{
    if (event.isUserInput) {
      if (event.source.selected){
        this.filterCourses.push(event.source.value);
      }else{
        const index = this.filterCourses.indexOf(event.source.value);
        if (index > -1) {
          this.filterCourses.splice(index, 1);
        }
      }
      this.getData();
    }
  }
}
