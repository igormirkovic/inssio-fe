import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatTableModule} from '@angular/material/table';
import {FormsModule} from '@angular/forms';
import { FormComponent } from './form/form.component';
import {AngularToastifyModule, ToastService} from 'angular-toastify';
import { BrowserModule } from '@angular/platform-browser';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';


@NgModule({
  declarations: [
    AppComponent,
    FormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatTableModule,
    HttpClientModule,
    FormsModule,
    AngularToastifyModule,
    MatFormFieldModule,
    MatSelectModule
  ],
  providers: [ToastService],
  bootstrap: [AppComponent]
})
export class AppModule { }
