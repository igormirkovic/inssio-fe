import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

export class Student {
  studentID: number;
  firstName: string;
  lastName: string;
  gender: string;
  address: string;
  state: string;
  city: string;
  dateOfBirth: string;
}

export class Course {
  courseID: number;
  code: string;
  name: string;
  description: string;
}

export class Mark{
  student: Student;
  course: Course;
  grade: number;
}

@Injectable({ providedIn: 'root' })

export class AppService{
  baseUrl = 'https://localhost:44380/';

  constructor(private http: HttpClient){}

  getStudents(): Observable<Student>{
    return this.http.get<Student>(this.baseUrl + 'students');
  }

  getStudentByID(id): Observable<Student>{
    return this.http.get<Student>(this.baseUrl + `students/${id}`);
  }

  getStudentsByIDs(ids): Observable<Student>{
    return this.http.post<Student>(this.baseUrl + 'students/filter', ids);
  }

  getCourses(): Observable<Course>{
    return this.http.get<Course>(this.baseUrl + 'courses');
  }

  getCourseByID(id): Observable<Student>{
    return this.http.get<Student>(this.baseUrl + `courses/${id}`);
  }

  getCoursesByIDs(ids): Observable<Course>{
    return this.http.post<Course>(this.baseUrl + 'courses/filter', ids);
  }

  getMarks(): Observable<Mark>{
    return this.http.get<Mark>(this.baseUrl + 'marks');
  }

  addStudent(model: any): any{
    return this.http.post(this.baseUrl + 'students', model);
  }

  addCourse(model: any): any{
    return this.http.post(this.baseUrl + 'courses', model);
  }

  editStudent(model): any{
    return this.http.put(this.baseUrl + 'students', model);
  }

  editCourse(model): any{
    return this.http.put(this.baseUrl + 'courses', model);
  }

  addMark(mark): any{
    return this.http.post(this.baseUrl + 'marks', mark);
  }

  editMark(mark): any{
    return this.http.put(this.baseUrl + 'marks', mark);
  }

  deleteCourse(id): any{
    return this.http.delete<Course>(this.baseUrl + `courses/${id}`);
  }

  deleteStudent(id): any{
    return this.http.delete<Student>(this.baseUrl + `students/${id}`);
  }
}

